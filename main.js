const table = document.createElement('table');

document.body.prepend(table);
table.style.border = '3px solid #a2a0a0';

(function fillingTable() {
    for (let i=0; i<30; i++){
        const tableRow = document.createElement('tr');
        tableRow.style.height = '22px';
        table.appendChild(tableRow);
        for (let j=0; j<30; j++){
            const tableData = document.createElement('td');
            tableData.classList.add('table-data');
            tableData.style.width = '22px';
            tableData.style.border = 'solid 2px #9E9E9E';
            tableRow.appendChild(tableData);
        }
    }
})();

document.body.addEventListener('click', function(event) {
    let tableData = document.querySelectorAll('.table-data');
    if (event.target === this) {
        table.classList.toggle('active');
        tableData.forEach(function(element){
            element.classList.remove('active');
        })
    } else {
        event.target.classList.toggle('active');
    }
});


